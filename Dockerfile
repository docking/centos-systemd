# centos-systemd

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.4
ARG CENTOS_VERSION=7


FROM ${DOCKER_REGISTRY_URL}centos-systemd-mini:${CUSTOM_VERSION}-${CENTOS_VERSION} AS centos-systemd

WORKDIR /

RUN \
	yum install -y \
		cron logrotate rsyslog \
	; \
	yum clean all ; \
	rpmdb --rebuilddb

RUN \
	sed -i \
		-e 's|^.*\(module(load="imklog"\)|#\1|' \
		/etc/rsyslog.conf

STOPSIGNAL SIGRTMIN+3

VOLUME /run /run/lock /tmp /sys/fs/cgroup

CMD exec /sbin/init
